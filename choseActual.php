<?php 
require_once 'functions.php';
require_once "init.php";
require_once "username.php";

$id_item = (int) $_POST['id_item'];
$checkActual = $con->prepare("SELECT actual FROM cards WHERE id = :id");
$checkActual->execute(['id'=>$id_item]);
$checkActual = $checkActual ->fetch(PDO::FETCH_ASSOC);

if($checkActual['actual'] == 0){
    $choseActual = $con->prepare("UPDATE `cards` SET actual = 1 WHERE id = :id");
    $choseActual->execute(['id'=>$id_item]);
}
else{
    $choseActual = $con->prepare("UPDATE `cards` SET actual = 0 WHERE id = :id");
    $choseActual->execute(['id'=>$id_item]);
}
if($checkActual['actual'] == 0){
    echo json_encode(array('result' => 'success1'));
}
else{
    echo json_encode(array('result' => 'success2'));
}
?>