<?php
require_once 'functions.php';
require_once "init.php";
require_once "username.php";

$key = $_GET['key'] ?? null;


$card = $con->prepare("SELECT * FROM cards WHERE id = :id");
$card->execute(['id'=>$key]);
$card = $card->fetch(PDO::FETCH_ASSOC);


$page_content = shablon(
    'card',
    [
        'card' => $card,
        'key' => $key, 
        'username' => $username,
    ]
);
echo shablon(
    'layout',
    [
        'page_content' =>  $page_content,
        'title' => 'Просмотр объявления ' . $card['card_name'],
        'username' => $username,
    ]
);
?>