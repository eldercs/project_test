<?php
require_once 'functions.php';
require_once "init.php";
require_once "username.php";
if($username == null){
    header("Location: index.php");
    exit();
}
$errors = [];
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $cardPost = $_POST;
    /* print_r($cardPost); */
    $requared = ['name', 'adress', 'description'];
    $numeric = ['price'];
    foreach ($requared as $name) {
        if (!array_key_exists($name, $cardPost) || empty($cardPost[$name])) {
            $errors[$name] = 'Это поле надо заполнить';
        }
    }
    foreach ($numeric as $name) {
        if (!is_numeric($cardPost[$name]) || intval($cardPost[$name]) <= 0) {
            $errors[$name] = 'Введите число больше нуля';
        }
    }
    if (!empty($_FILES['img']['name'])) {
        $tmpName = $_FILES['img']['tmp_name'];
        $folder = 'img/uploads/';
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }
        $path = $folder . time() . $_FILES['img']['name'];
        $fileType = mime_content_type($tmpName);
        if ($fileType !== "image/jpeg" && $fileType !== "image/png") {
            $errors['img'] = 'Загрузите картинку в формате jpg или png';
        } else {
            move_uploaded_file($tmpName, $path);
            $cardPost['img'] = $path;
        }
    } else {
        $errors['img'] = 'Вы не загрузили файл';
    }
    /* print_r($errors); */
    if(count($errors)){
        $page_content = shablon('create_card',
        [
            'errors' => $errors,
        ]);
    }
    else{
        $cardPost = array_map('htmlspecialchars', $cardPost);
        $sql = "INSERT INTO `cards` (`user_id`, `card_name`, `adress`, `price`, `description`,`image`, `actual`) VALUES ('$username[id]', ?, ?, ?, ?, ?, '1')";
        $add_card = $con->prepare($sql);
        $add_card -> execute([$cardPost['name'], $cardPost['adress'], $cardPost['price'],$cardPost['description'], $cardPost['img']]);
        header("Location: index.php");
    }   
}
$page_content = shablon(
    'create_card',
    [    
        'errors' => $errors,
    ]
); 
echo shablon(
    'layout',
    [
        'username' => $username,
        'page_content' =>  $page_content, 
        'title' => 'Добавление объявления',
    ]
);
