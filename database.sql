CREATE DATABASE `Rieltor`;

USE `Rieltor`;

CREATE TABLE users(
    id INT AUTO_INCREMENT PRIMARY KEY,
    login VARCHAR(128),
    password VARCHAR(120),
    role INT(10)
);

CREATE TABLE cards(
    id INT AUTO_INCREMENT PRIMARY KEY,
    card_name VARCHAR(100),
    adress VARCHAR(128),
    price INT(20),
    description VARCHAR(1000),
    image VARCHAR(400),
    actual INT(10),
    user_id INT(11),
    KEY `user_id` (`user_id`),
    CONSTRAINT `user_fk_2` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`)
);