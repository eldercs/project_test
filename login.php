<?php
require_once 'functions.php';
require_once 'init.php';
require_once "username.php";
$errors = [];
if($_SERVER['REQUEST_METHOD']=='POST'){
    $requared = ['login', 'password'];
    $userPost = $_POST;
    foreach($requared as $name){
        if(empty($userPost[$name])){
            $errors[$name] = "Это поле надо заполнить";
        }
    }
    if(!count($errors)){
        $checkUser = $con->prepare("SELECT * FROM users WHERE login = :login AND password = :password");
        $checkUser->execute(array('login'=>$userPost['login'], 'password'=>$userPost['password'])); 
        $userExists = $checkUser->fetch(PDO::FETCH_ASSOC);
        if($userExists){
            $_SESSION['user'] = $userExists;
            /* print_r($userExists); */
            header("Location: index.php");
            exit(); 
        }else {
            $errors['all'] = 'Вы ввели неверный логин/пароль';
        }
        /* print_r($userExists); */
    }
}
$page_content = shablon(
    'login',
    [   
        'errors' => $errors
    ]
); 
echo shablon(
    'layout',
    [   
        'username' => $username,
        'page_content' =>  $page_content, 
        'title' => 'Авторизация',
    ]
);
?>