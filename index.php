<?php
require_once 'functions.php';
require_once 'init.php';
require_once "username.php";

$checkRole = $con->prepare("SELECT * FROM users WHERE login = :login");
$checkRole->execute(['login'=>$username['login']]);
$checkRole = $checkRole ->fetch(PDO::FETCH_ASSOC);
$cards = $con->query("SELECT * FROM cards");

$page_content = shablon(
    'index',
    [   
        'role'=> $checkRole['role'],
        'cards' => $cards,
        'username' => $username
    ]
);
echo shablon(
    'layout',
    [
        'username' => $username,
        'page_content' =>  $page_content,
        'title' => 'Реилторное агенство',
    ]
);
?>