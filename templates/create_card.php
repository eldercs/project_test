<form class="form form--add-lot add-container form--invalid" action="create_card.php" method="post" enctype="multipart/form-data">
    <h2>Добавление объявления</h2>
    <div class="form__container">
        <div class="form__item">
            <label for="name" class="form-label">Название</label>
            <input id="name" class="form-control" type="text" name="name" placeholder="Введите название объекта" required>
        </div>
        <div class="form__item">
            <label for="adress" class="form-label">Адрес</label>
            <input id="adress" class="form-control" type="text" name="adress" placeholder="Введите адрес" required>
        </div>
        <div class="form__item">
            <label for="price" class="form-label">Цена</label>
            <input id="price" class="form-control" type="number" name="price" placeholder="0" required>
        </div>
        <div class="form__item form__item-description">
            <label for="description" class="form-label">Описание</label>
            <textarea id="description"class="form-control" name="description" placeholder="Напишите описание номера" required></textarea>
        </div>
        <div class="form__item form__item--file form__item--last">
            <label>Изображение</label>
            <br>
            <img id="output" src="" onload="alert('Файл существует!');" max-height="200" width="250" />
            <input type='file' id="input__file" name="img" class="input input__file" onchange="loadFile(event, 1)" />
            <!-- <label for="input__file" class="input__file-button">
                <span class="input__file-icon-wrapper"><img class="input__file-icon" src="./src/img/add.png" alt="Выбрать файл" width="40"></span>
                <span class="input__file-button-text">Выберите файл</span>
            </label> -->
        </div>
        <button type="submit" class="btn btn-outline-primary btn-add">Добавить объявление</button>
    </div>
</form>
<script>
  var loadFile = function(event, id) {
    if(id == 1)
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src)
    }
  };
</script>