<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script type = "text/javascript" src = "https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <link rel="stylesheet"  href="/styles.css" />
</head>

<body>
    <header class="main-header">
        <div class="main-header__container">
            <nav>
                <?php if ($username) : ?>
                    <ul class="main-header__navigate">
                        <li class="navigate-item"><a class="btn btn-secondary" href="/create_card.php">Создать объявление</a></li>
                        <li class="navigate-item"><p class="nav-link"><?= $username['login']; ?><br><a href="/logout.php">Выход</a></p></li>
                    </ul>
                <?php else: ?>
                <ul class="main-header__navigate">
                    <li class="navigate-item">
                        <a href="/login.php">Авторизация</a>
                    </li>
                </ul>
                <?php endif; ?>
            </nav>
        </div>
    </header>
    <main class="container">
        <?= $page_content; ?>
    </main>
    <footer class="main-footer">
        <div class="main-footer__bottom container">
        </div>
    </footer>

</body>

</html>