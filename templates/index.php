<script>
    $(document).ready(function() {
        $('button#actual').click(function() {
            choseActual('actual', $(this));
        });
    });

    function choseActual(type, element) {
        var id_item = $('#id_item').val();
        $.ajax({
            type: "POST",
            url: "/choseActual.php",
            data: {
                'id_item': id_item,
                'type': type
            },
            dataType: "json",
            success: function(data) {
                if (data.result == 'success1') {
                    document.getElementById("actual").innerHTML = "Актуально"
                } else {
                    document.getElementById("actual").innerHTML = "Не актуально"
                }
            }
        });
    }
</script>
<div>
    <h2>Список объявлений</h2>
    <div class="card__list">
        <?php foreach ($cards as $val) : ?>
            <? if ($username && $role == 1) { ?>
                <div class="card__block">
                    <? if ($val['actual'] == 1) { ?>
                        <p id='actual'>Актуально</p>
                    <? } else { ?>
                        <p id='actual'>Не актуально</p>
                    <? } ?>
                    <img src="<?= $val['image']; ?>" class="image" width="320" height="240">
                    <p>Название: <a href="card.php?key=<?= $val['id']; ?>"><?= $val['card_name']; ?></a></p>
                    <p>Адрес:<?= $val['adress']; ?></p>
                    <p>Стоимость:<?= $val['price']; ?>р.</p>
                    <input type="hidden" id="id_item" value="<?= $val['id']; ?>" />
                    <button id="actual" class="btn btn-outline-primary">Изменить актуальность</button>
                </div>
            <? } else if (!$username && $val['actual'] == 1) { ?>
                <div class="card__block">
                    <p>Актуально</p>
                    <img src="<?= $val['image']; ?>" class="image" width="320" height="240">
                    <p>Название: <a href="card.php?key=<?= $val['id']; ?>"><?= $val['card_name']; ?></a></p>
                    <p>Адрес:<?= $val['adress']; ?></p>
                    <p>Стоимость:<?= $val['price']; ?></p>
                </div>
            <? } ?>
        <?php endforeach ?>
    </div>
</div>