<form class="form container-login"  method="post">
    <div>
        <h2>Авторизация</h2>
        <div class="form__item">
            <label for="login" class="form-label">Логин</label>
            <input id="login" class ="login-input form-control" type="text" name="login" placeholder="Введите логин" required>
        </div>
        <div class="form__item">
            <label for="password" class="form-label">Пароль</label>
            <input id="password" class ="password-input form-control" type="password" name="password" placeholder="Введите пароль" required>
        </div>
        <button class = "btn btn-outline-primary login-btn" type="submit">Войти</button>
    </div>
</form>